#!/usr/bin/env python

"""Set modified date of all files in a directory to the last time they were committed."""

import os
import subprocess
import sys
import getopt

# Number of files processed
count = 0

# Extra detail or not
verbose = False


def setMtime(subdir, file, count):
    """Set the modification datetime of a file to the datetime of the last committed change to it."""

    # Magic git command to get ISO date for last commit of file
    cmd = ["git", "-C", subdir, "log", "-1", "--format=%ct", "--", file]
    # Objects not under git control will return an empty string here
    mtime = subprocess.check_output(cmd)
    if mtime:
        mtime = int(mtime)
        # Some objects (e.g. symlinks) cannot have their modified time changed
        try:
            fname = os.path.join(subdir, file)
            if verbose:
                print(fname)
            os.utime(fname, (mtime, mtime))
            count += 1
        except OSError:
            pass
    return count


def showUsage():
    print('Usage:  git-mtime [options] [dir]')
    print('        descends from dir, applying the last commit date to each file')
    print('Options: -h --help     show this help')
    print('         -v --verbose  be chatty about stamping')


if __name__ == "__main__":
    # Check for switches
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hv", ["help", "verbose"])
    except getopt.GetoptError as err:
        print(str(err))
        showUsage()
        exit(1)

    for o, a in opts:
        if o in ("-h", "--help"):
            showUsage()
            exit(0)
        elif o in ("-v", "--verbose"):
            verbose = True

    # Directory to process is our parameter, or we default to "." if none
    try:
        dname = args[0]
    except IndexError:
        dname = "."
    if verbose:
        print('Stamping from ' + dname)

    for subdir, dirs, files in os.walk(dname):
        # Save time and ignore everything in the .git directory tree!
        if "/.git/" not in subdir:
            for file in files:
                if file != ".git":
                    count = setMtime(subdir, file, count)

    if verbose:
        print("Processed " + str(count) + " files")
    exit(0)
